﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour {
    // Start is called before the first frame update
    [SerializeField]
    protected Item _itemType;

    public Item Type {
        get {
            return _itemType;
        }
        set {
            _itemType = value;
        }

    }

    // Update is called once per frame
    protected int _locationIndex;
    public int LocationIndex {
        get {
            return _locationIndex;
        }
        set {
            _locationIndex = value;
        }

    }
}